package com.company;
import com.company.character.classes.Mage;

public class Main {
    public static void main(String[] args) {
        Mage newMage = new Mage("Ante the wizard");
        newMage.levelUp();
        newMage.printCharacterSheet();
    }
}