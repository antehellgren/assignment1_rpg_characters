package com.company.character;

public class SecondaryAttributes {
    private int health;
    private int armorRating;
    private int elementalResistance;

    public SecondaryAttributes(PrimaryAttributes primaryAttributes) {
        this.updateSecondaryAttributes(primaryAttributes);
    }
    public void updateSecondaryAttributes(PrimaryAttributes primaryAttributes) {
        this.health = primaryAttributes.getVitality() * 10;
        this.armorRating = primaryAttributes.getStrength() + primaryAttributes.getDexterity();
        this.elementalResistance = primaryAttributes.getIntelligence();
    }
    public void listSecondaryAttributes() {
        System.out.println("Health: " + health +", Armor: " + armorRating + ", Resistance: " + elementalResistance);
    }

    public int getHealth() {
        return health;
    }

    public int getArmorRating() {
        return armorRating;
    }

    public int getElementalResistance() {
        return elementalResistance;
    }
}
