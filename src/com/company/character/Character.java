package com.company.character;

import com.company.equipment.*;
import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;

import java.util.HashMap;
import java.util.TreeMap;

public abstract class Character {
    private String name;
    private int level = 1;
    private double damagePerSecond = 1;
    protected PrimaryAttributes baseAttributes;
    protected SecondaryAttributes secondaryAttributes;
    protected HashMap<Slot, Item> characterEquipment = new HashMap<>();
    protected Class classType;

    public Character(String name, Class classType) {
        this.name = name;
        this.baseAttributes = new PrimaryAttributes(classType.getAttributes());
        this.secondaryAttributes = new SecondaryAttributes(baseAttributes);
        this.classType = classType;
        updateDamagePerSecond();
    }

    public HashMap<Slot, Item> getCharacterEquipment() {
        return characterEquipment;
    }

    public PrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }

    public void updateItemAttributes() {
        TreeMap<AttributeConstants, Integer> itemAttributes = new TreeMap<>();
        TreeMap<AttributeConstants, Integer> totalItemAttributes = new TreeMap<>();
        int vitality = 0;
        int strength = 0;
        int dexterity = 0;
        int intelligence = 0;

        for (Item item :
                characterEquipment.values()) {
            if (item.getArmorType() != null) {
                itemAttributes.putAll(((Armor) item).getAttributes()); // Armor stats treemap
                if (itemAttributes.get(AttributeConstants.vitality) != null) {
                    vitality += itemAttributes.get(AttributeConstants.vitality); // save current stats for next loop
                    strength += itemAttributes.get(AttributeConstants.strength);
                    dexterity += itemAttributes.get(AttributeConstants.dexterity);
                    intelligence += itemAttributes.get(AttributeConstants.intelligence);
                }
            }
        }
        totalItemAttributes.put(AttributeConstants.vitality, vitality);
        totalItemAttributes.put(AttributeConstants.strength, strength);
        totalItemAttributes.put(AttributeConstants.dexterity, dexterity);
        totalItemAttributes.put(AttributeConstants.intelligence, intelligence);
        baseAttributes.setTotalAttributes(totalItemAttributes);
    }

    public SecondaryAttributes getSecondaryAttributes() {
        return secondaryAttributes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level)  {
        if(level < 1) {
            throw new IllegalArgumentException();
        } else {
            this.level = level;
        }
    }

    public boolean equipWeapon(Item item) throws InvalidWeaponException {
        switch (item.getWeaponType()) {
            case Axe:
            case Hammer:
                if (classType == Class.Warrior && item.getRequiredLevel() <= getLevel()) {
                    this.damagePerSecond = 0;
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidWeaponException("Weapon too high level!");
                }
                break;
            case Bow:
                if (classType == Class.Ranger && item.getRequiredLevel() <= getLevel()) {
                    this.damagePerSecond = 0;
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidWeaponException("Weapon too high level!");
                }
                break;
            case Wand:
            case Staff:
                if (classType == Class.Mage && item.getRequiredLevel() <= getLevel()) {
                    this.damagePerSecond = 0;
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidWeaponException("Weapon too high level!");
                }
                break;
            case Sword:
                if (classType == Class.Warrior || classType == Class.Rogue && item.getRequiredLevel() <= getLevel()) {
                    this.damagePerSecond = 0;
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidWeaponException("Weapon too high level!");
                }
                break;
            case Dagger:
                if (classType == Class.Rogue && item.getRequiredLevel() <= getLevel()) {
                    this.damagePerSecond = 0;
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidWeaponException("Weapon too high level!");
                }
                break;
        }
        updateDamagePerSecond();
        return true;
    }

    public Item unequipSlot(Slot slot) {
        Item itemRemoved = characterEquipment.remove(slot);
        System.out.println(itemRemoved);
        updateItemAttributes();
        updateDamagePerSecond();
        return itemRemoved;
    }

    public boolean equipArmor(Item item) throws InvalidArmorException {
        switch (item.getArmorType()) {
            case Cloth -> {
                if (classType == Class.Mage && item.getRequiredLevel() <= getLevel()) {
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidArmorException("Too low level to equip " + item.getName());
                }
                break;
            }
            case Leather -> {
                if (classType == Class.Rogue || classType == Class.Ranger && item.getRequiredLevel() <= getLevel()) {
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidArmorException("Too low level to equip " + item.getName());
                }
                break;
            }
            case Mail -> {
                if (classType == Class.Ranger || classType == Class.Rogue || classType == Class.Warrior && item.getRequiredLevel() <= getLevel()) {
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidArmorException("Too low level to equip " + item.getName());
                }
                break;
            }
            case Plate -> {
                if (classType == Class.Warrior && item.getRequiredLevel() <= getLevel()) {
                    characterEquipment.put(item.getItemSlot(), item);
                } else {
                    throw new InvalidArmorException("Too low level to equip " + item.getName());
                }
                break;
            }
        }
        updateItemAttributes();
        updateDamagePerSecond();
        return true;
    }

    public void listEquippedItems() {
        for (Slot i : characterEquipment.keySet()) {
            System.out.println(i + " : " + characterEquipment.get(i).getName());
        }
    }

    public void increaseLevel() {
        level++;
        System.out.println(name + " has gained a level! New level: " + level + ". Attributes increased.");
    }

    public double updateDamagePerSecond() {
        switch (classType) {
            case Mage -> {
                if (baseAttributes.getTotalAttributes() != null)
                    this.damagePerSecond = baseAttributes.getTotalAttributes().get(AttributeConstants.intelligence);
            }
            case Rogue, Ranger -> {
                if(baseAttributes.getTotalAttributes() != null)
                    this.damagePerSecond = baseAttributes.getTotalAttributes().get(AttributeConstants.dexterity);
            }
            case Warrior -> {
                if (baseAttributes.getTotalAttributes() != null)
                    this.damagePerSecond = baseAttributes.getTotalAttributes().get(AttributeConstants.strength);
            }
        }
        if (characterEquipment.get(Slot.Weapon) != null) {
            this.damagePerSecond = damagePerSecond + ((Weapon) characterEquipment.get(Slot.Weapon)).getWeaponDamagePerSecond();
        }
        return damagePerSecond;
    }

    public double getDamagePerSecond() {
        return damagePerSecond;
    }
    public void printCharacterSheet() {
        StringBuilder stats = new StringBuilder("Character name: " + this.name + "\n Level: " + this.level +
                "\n Strength: " + baseAttributes.getStrength() + "\n Dexterity: " + baseAttributes.getDexterity() +
                "\n Intelligence: " + baseAttributes.getIntelligence() + "\n Health: " + secondaryAttributes.getHealth() +
                "\n Armor Rating: " + secondaryAttributes.getArmorRating() + "\n Elemental Resistance: " +
                secondaryAttributes.getElementalResistance() + "\n Damage per Second: " + this.damagePerSecond);
        System.out.println(stats);
    }
}