package com.company.character;

import java.util.TreeMap;

public enum Class {
    Mage(5, 1, 1, 8),
    Ranger(8,1,7,1),
    Rogue(8,2,6,1),
    Warrior(10,5,2,1);

    private final int vitality;
    private final int strength;
    private final int dexterity;
    private final int intelligence;

    Class(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public TreeMap getAttributes() {
        TreeMap<AttributeConstants, Integer> attributes = new TreeMap<>();

        attributes.put(AttributeConstants.vitality, vitality);
        attributes.put(AttributeConstants.strength, strength);
        attributes.put(AttributeConstants.dexterity, dexterity);
        attributes.put(AttributeConstants.intelligence, intelligence);

        return attributes;
    }

}
