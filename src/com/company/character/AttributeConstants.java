package com.company.character;

public enum AttributeConstants {
    vitality,
    strength,
    dexterity,
    intelligence;
}
