package com.company.character.classes;

import com.company.character.Character;
import com.company.character.Class;

public class Mage extends Character {

    public Mage(String name) {
        super(name, Class.Mage);

        setName(name);
    }

    public void levelUp() {
        increaseLevel();
        baseAttributes.increaseAttributes(3, 1, 1, 5);
        baseAttributes.listPrimaryAttributes();
        secondaryAttributes.updateSecondaryAttributes(baseAttributes);
        secondaryAttributes.listSecondaryAttributes();
        updateDamagePerSecond();
    }

}
