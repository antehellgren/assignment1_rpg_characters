package com.company.character.classes;

import com.company.character.Character;
import com.company.character.Class;

public class Ranger extends Character {

    public Ranger(String name) {
        super(name, Class.Ranger);
        setName(name);
    }

    public void levelUp() {
        increaseLevel();
        baseAttributes.increaseAttributes(2, 1, 5, 1);
        baseAttributes.listPrimaryAttributes();
        secondaryAttributes.updateSecondaryAttributes(baseAttributes);
        secondaryAttributes.listSecondaryAttributes();
        updateDamagePerSecond();
    }

}
