package com.company.character.classes;

import com.company.character.Character;
import com.company.character.Class;

public class Warrior extends Character {

    public Warrior(String name) {
        super(name, Class.Warrior);

        setName(name);
    }

    public void levelUp() {
        increaseLevel();
        baseAttributes.increaseAttributes(5, 3, 2, 1);
        baseAttributes.listPrimaryAttributes();
        secondaryAttributes.updateSecondaryAttributes(baseAttributes);
        secondaryAttributes.listSecondaryAttributes();
        updateDamagePerSecond();
    }

}
