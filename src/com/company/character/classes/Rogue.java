package com.company.character.classes;

import com.company.character.Character;
import com.company.character.Class;

public class Rogue extends Character {

    public Rogue(String name) {
        super(name, Class.Rogue);
        setName(name);
    }

    public void levelUp() {
        increaseLevel();
        baseAttributes.increaseAttributes(3, 1, 4, 1);
        baseAttributes.listPrimaryAttributes();
        secondaryAttributes.updateSecondaryAttributes(baseAttributes);
        secondaryAttributes.listSecondaryAttributes();
        updateDamagePerSecond();
    }

}
