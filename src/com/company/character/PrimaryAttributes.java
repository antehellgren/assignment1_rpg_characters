package com.company.character;

import java.util.TreeMap;

public class PrimaryAttributes {
    private int vitality;
    private int strength;
    private int dexterity;
    private int intelligence;
    private TreeMap<AttributeConstants, Integer> attributes;
    private TreeMap<AttributeConstants, Integer> totalAttributes;

    public PrimaryAttributes(TreeMap attributes) {
        updateAttributes(attributes);
    }
    public void updateAttributes(TreeMap attributes) {
        this.vitality = (int) attributes.get(AttributeConstants.vitality);
        this.strength = (int) attributes.get(AttributeConstants.strength);
        this.dexterity = (int) attributes.get(AttributeConstants.dexterity);
        this.intelligence = (int) attributes.get(AttributeConstants.intelligence);
        this.attributes = attributes;
    }

    /**
     *
     * @param totalItemAttributes Total sum of all equipped items.
     * @desc Sums up item attributes with class base attributes to create total attributes treemap.
     */
    public void setTotalAttributes(TreeMap<AttributeConstants, Integer> totalItemAttributes) {
        totalAttributes = new TreeMap<>();
        totalAttributes.putAll(totalItemAttributes);
        AttributeConstants _vitality = AttributeConstants.vitality;
        AttributeConstants _strength = AttributeConstants.strength;
        AttributeConstants _dexterity = AttributeConstants.dexterity;
        AttributeConstants _intelligence = AttributeConstants.intelligence;

        totalAttributes.put(_vitality, totalAttributes.get(_vitality)+attributes.get(_vitality));
        totalAttributes.put(_strength, totalAttributes.get(_strength)+attributes.get(_strength));
        totalAttributes.put(_dexterity, totalAttributes.get(_dexterity)+attributes.get(_dexterity));
        totalAttributes.put(_intelligence, totalAttributes.get(_intelligence)+attributes.get(_intelligence));

    }
    public TreeMap<AttributeConstants, Integer> getTotalAttributes() {
        return totalAttributes;
    }

    public void listPrimaryAttributes() {
        System.out.println("Vitality: " + vitality + ", Strength: " + strength + ", Dexterity: " + dexterity + ", Intelligence: " + intelligence);
    }
    public void increaseAttributes(int vitality, int strength, int dexterity, int intelligence) {
        AttributeConstants _vitality = AttributeConstants.vitality;
        AttributeConstants _strength = AttributeConstants.strength;
        AttributeConstants _dexterity = AttributeConstants.dexterity;
        AttributeConstants _intelligence = AttributeConstants.intelligence;

        attributes.put(_vitality, attributes.get(_vitality)+vitality);
        attributes.put(_strength, attributes.get(_strength)+strength);
        attributes.put(_dexterity, attributes.get(_dexterity)+dexterity);
        attributes.put(_intelligence, attributes.get(_intelligence)+intelligence);
        updateAttributes(attributes);
    }

    public int getVitality() {
        return vitality;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }
}
