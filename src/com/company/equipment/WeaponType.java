package com.company.equipment;

public enum WeaponType {
    Axe,
    Bow,
    Dagger,
    Hammer,
    Staff,
    Sword,
    Wand,
}
