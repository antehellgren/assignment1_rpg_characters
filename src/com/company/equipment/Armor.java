package com.company.equipment;

import com.company.character.AttributeConstants;

import java.util.TreeMap;

public class Armor extends Item {

    private TreeMap<AttributeConstants, Integer> attributes = new TreeMap<>();

    public Armor(String name, int requiredLevel, Slot itemSlot, ArmorType armorType, TreeMap<AttributeConstants, Integer> attributes) {
        super(name, requiredLevel, itemSlot, armorType, attributes);

        this.attributes.putAll(attributes);
    }
    public TreeMap<AttributeConstants, Integer> getAttributes() {
        return attributes;
    }
}
