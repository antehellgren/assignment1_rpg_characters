package com.company.equipment;


public enum ArmorType {
    Cloth(),
    Leather(),
    Mail(),
    Plate();

}
