package com.company.equipment;

public class Weapon extends Item {
    private double damage;
    private double attackSpeed;

    public Weapon(String name, int requiredLevel, Slot itemSlot, WeaponType weaponType, double damage, double attackSpeed) {
        super(name, requiredLevel, itemSlot, weaponType);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }
    public double getWeaponDamagePerSecond() {
        return (damage * attackSpeed);
    }
}
