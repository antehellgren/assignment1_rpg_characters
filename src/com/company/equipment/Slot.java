package com.company.equipment;

public enum Slot {
    Head,
    Body,
    Legs,
    Weapon
}
