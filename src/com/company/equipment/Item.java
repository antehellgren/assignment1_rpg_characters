package com.company.equipment;

import com.company.character.AttributeConstants;

import java.util.TreeMap;

public abstract class Item {
    private String name;
    private int requiredLevel;
    private Slot itemSlot;
    private WeaponType weaponType;
    private ArmorType armorType;
    private TreeMap attributes;

    public Item(String name, int requiredLevel, Slot itemSlot, WeaponType weaponType) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.itemSlot = itemSlot;
        this.weaponType = weaponType;
    }
    public Item(String name, int requiredLevel, Slot itemSlot, ArmorType armorType, TreeMap<AttributeConstants, Integer> attributes) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.itemSlot = itemSlot;
        this.armorType = armorType;
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getItemSlot() {
        return itemSlot;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public ArmorType getArmorType() {
        return armorType;
    }
}
