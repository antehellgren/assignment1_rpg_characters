package com.company.character.classes;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;



public class AttributeTest {
    @Test
    void isMageDefaultAttributesCorrect_whenCreated_correctNumberForClass() {
        var mage = new Mage("Mage");

        boolean isAttributesCorrect = false;
        if(mage.getBaseAttributes().getVitality() == 5 &&
                mage.getBaseAttributes().getStrength() == 1 &&
                mage.getBaseAttributes().getDexterity() == 1 &&
                mage.getBaseAttributes().getIntelligence() == 8)
            isAttributesCorrect = true;
        assertTrue(isAttributesCorrect);
    }

    @Test
    void isMageAttributesUpdating_whenLevelUp_correctNumberForClass() {
        int i;
        var mage = new Mage("Mage");
        for (i = 0; i < 1; i++) {
            mage.levelUp();
        }
        boolean isAttributeIncreaseCorrect = false;
        if(mage.getBaseAttributes().getVitality() == i*3+5 &&
                mage.getBaseAttributes().getStrength() == i+1 &&
                mage.getBaseAttributes().getDexterity() == i+1 &&
                mage.getBaseAttributes().getIntelligence() == i*5+8)
            isAttributeIncreaseCorrect = true;
        assertTrue(isAttributeIncreaseCorrect);
    }

    @Test
    void isWarriorDefaultAttributesCorrect_whenCreated_correctNumberForClass() {
        var warrior = new Warrior("Warrior");
        boolean isAttributesCorrect = false;
        if(warrior.getBaseAttributes().getVitality() == 10 &&
                warrior.getBaseAttributes().getStrength() == 5 &&
                warrior.getBaseAttributes().getDexterity() == 2 &&
                warrior.getBaseAttributes().getIntelligence() == 1)
            isAttributesCorrect = true;
        assertTrue(isAttributesCorrect);
    }

    @Test
    void isWarriorAttributesUpdating_whenLevelUp_correctNumberForClass() {
        int i;
        var warrior = new Warrior("Warrior");
        for (i = 0; i < 10; i++) {
            warrior.levelUp();
        }
        boolean isAttributeIncreaseCorrect = false;
        if(warrior.getBaseAttributes().getVitality() == i*5+10 &&
                warrior.getBaseAttributes().getStrength() == i*3+5 &&
                warrior.getBaseAttributes().getDexterity() == i*2+2 &&
                warrior.getBaseAttributes().getIntelligence() == i+1)
            isAttributeIncreaseCorrect = true;
        assertTrue(isAttributeIncreaseCorrect);
    }

    @Test
    void isRogueDefaultAttributesCorrect_whenCreated_correctNumberForClass() {
        var rogue = new Rogue("Rogue");
        boolean isAttributesCorrect = false;
        if(rogue.getBaseAttributes().getVitality() == 8 &&
                rogue.getBaseAttributes().getStrength() == 2 &&
                rogue.getBaseAttributes().getDexterity() == 6 &&
                rogue.getBaseAttributes().getIntelligence() == 1)
            isAttributesCorrect = true;
        assertTrue(isAttributesCorrect);
    }
    @Test
    void isRogueAttributesUpdating_whenLevelUp_correctNumberForClass() {
        int i;
        var rogue = new Rogue("Rogue");
        for (i = 0; i < 10; i++) {
            rogue.levelUp();
        }
        boolean isAttributeIncreaseCorrect = false;
        if(rogue.getBaseAttributes().getVitality() == i*3+8 &&
                rogue.getBaseAttributes().getStrength() == i+2 &&
                rogue.getBaseAttributes().getDexterity() == i*4+6 &&
                rogue.getBaseAttributes().getIntelligence() == i+1)
            isAttributeIncreaseCorrect = true;
        assertTrue(isAttributeIncreaseCorrect);
    }
    @Test
    void isRangerDefaultAttributesCorrect_whenCreated_correctNumberForClass() {
        var ranger = new Ranger("Ranger");
        boolean isAttributesCorrect = false;
        if(ranger.getBaseAttributes().getVitality() == 8 &&
                ranger.getBaseAttributes().getStrength() == 1 &&
                ranger.getBaseAttributes().getDexterity() == 7 &&
                ranger.getBaseAttributes().getIntelligence() == 1)
            isAttributesCorrect = true;
        assertTrue(isAttributesCorrect);
    }
    @Test
    void isRangerAttributesUpdating_whenLevelUp_correctNumberForClass() {
        int i;
        var ranger = new Ranger("Ranger");
        for (i = 0; i < 10; i++) {
            ranger.levelUp();
        }
        boolean isAttributeIncreaseCorrect = false;
        if(ranger.getBaseAttributes().getVitality() == i*2+8 &&
                ranger.getBaseAttributes().getStrength() == i+1 &&
                ranger.getBaseAttributes().getDexterity() == i*5+7 &&
                ranger.getBaseAttributes().getIntelligence() == i+1)
            isAttributeIncreaseCorrect = true;
        assertTrue(isAttributeIncreaseCorrect);
    }
}
