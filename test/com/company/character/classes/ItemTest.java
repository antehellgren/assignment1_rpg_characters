package com.company.character.classes;

import com.company.character.AttributeConstants;
import com.company.equipment.*;
import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

public class ItemTest {

    @Test
    void isExceptionThrownWhenWeaponTooHighLevel_highLevelWeapon_invalidWeaponException() throws InvalidWeaponException {
        var warrior = new Warrior("Warrior");
        Weapon mightyAxe = new Weapon("Mighty Axe", 2, Slot.Weapon, WeaponType.Axe, 10, 0.4);
        assertThrows(InvalidWeaponException.class, ()-> warrior.equipWeapon(mightyAxe));
    }

    @Test
    void isExceptionThrownWhenArmorTooHighLevel_highLevelArmor_invalidArmorException() throws InvalidArmorException {
        var warrior = new Warrior("Warrior");

        TreeMap<AttributeConstants, Integer> plateArmorStats = new TreeMap<>();
        plateArmorStats.put(AttributeConstants.vitality, 4);
        plateArmorStats.put(AttributeConstants.strength, 4);
        plateArmorStats.put(AttributeConstants.dexterity, 1);
        plateArmorStats.put(AttributeConstants.intelligence, 1);

        Armor mightyPlateArmor = new Armor("Mighty Plate chest", 2, Slot.Body,ArmorType.Plate, plateArmorStats);
        assertThrows(InvalidArmorException.class, ()-> warrior.equipArmor(mightyPlateArmor));
    }

    @Test
    void isExceptionThrownWhenWeaponWrongType_wrongTypeWeapon_invalidWeaponException() throws InvalidWeaponException {
        var warrior = new Warrior("Warrior");
        Weapon mightyAxe = new Weapon("Mighty Axe", 1, Slot.Weapon, WeaponType.Bow, 10, 0.4);
        assertThrows(InvalidWeaponException.class, ()-> warrior.equipWeapon(mightyAxe));
    }

    @Test
    void isExceptionThrownWhenArmorWrongType_wrongTypeArmor_invalidArmorException() throws InvalidArmorException {
        var warrior = new Warrior("Warrior");
        TreeMap<AttributeConstants, Integer> plateArmorStats = new TreeMap<>();
        plateArmorStats.put(AttributeConstants.vitality, 4);
        plateArmorStats.put(AttributeConstants.strength, 4);
        plateArmorStats.put(AttributeConstants.dexterity, 1);
        plateArmorStats.put(AttributeConstants.intelligence, 1);
        Armor mightyPlateArmor = new Armor("Mighty Plate chest", 2, Slot.Body,ArmorType.Cloth, plateArmorStats);

        assertThrows(InvalidArmorException.class, ()-> warrior.equipArmor(mightyPlateArmor));
    }

    @Test
    void isEquipWeaponReturningTrue_equipWeapon_true() throws InvalidWeaponException {
        var warrior = new Warrior("Warrior");
        Weapon mightyAxe = new Weapon("Mighty Axe", 1, Slot.Weapon, WeaponType.Axe, 10, 0.4);
        assertTrue(warrior.equipWeapon(mightyAxe));
    }

    @Test
    void isDpsOneWithOutWeapon_null_One()  {
        var warrior = new Warrior("Warrior");
        double expected_dps = 1*(1 + (5 / 100));
        assertEquals(expected_dps, warrior.getDamagePerSecond());
    }

    @Test
    void isDpsCalculationCorrect_withWeapon_correctValues()  {
        var warrior = new Warrior("Warrior");
        Weapon mightyAxe = new Weapon("Mighty Axe", 1, Slot.Weapon, WeaponType.Axe, 7, 1.1);
        double expected_dps = (7 * 1.1)*(1 + (5 / 100));
        try {
            warrior.equipWeapon(mightyAxe);
        } catch (InvalidWeaponException e ) {
            System.out.println(e.getMessage());
        }
        assertEquals(expected_dps, warrior.getDamagePerSecond());
    }
    @Test
    void isDpsCalculationCorrect_withWeaponAndArmor_correctValues()  {
        var warrior = new Warrior("Warrior");

        TreeMap<AttributeConstants, Integer> plateArmorStats = new TreeMap<>();
        plateArmorStats.put(AttributeConstants.vitality, 4);
        plateArmorStats.put(AttributeConstants.strength, 4);
        plateArmorStats.put(AttributeConstants.dexterity, 1);
        plateArmorStats.put(AttributeConstants.intelligence, 1);

        Armor mightyPlateArmor = new Armor("Mighty Plate chest", 2, Slot.Body,ArmorType.Plate, plateArmorStats);
        Weapon mightyAxe = new Weapon("Mighty Axe", 1, Slot.Weapon, WeaponType.Axe, 7, 1.1);

        double expected_dps = (7 * 1.1) * (1 + ((5+1) / 100));
        try {
            warrior.equipWeapon(mightyAxe);
        } catch (InvalidWeaponException e ) {
            System.out.println(e.getMessage());
        }
        try {
            warrior.equipArmor(mightyPlateArmor);
        } catch(InvalidArmorException e ) {
            System.out.println(e.getMessage());
        }
        assertEquals(expected_dps, warrior.getDamagePerSecond());
    }
}
