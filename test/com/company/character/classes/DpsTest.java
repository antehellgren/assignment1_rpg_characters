package com.company.character.classes;

import com.company.character.AttributeConstants;
import com.company.equipment.*;
import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class DpsTest {
    @Test
    void isMageDPSUpdatingProperly_equippedItemsAndLevel_correctCalculations()  {
        var mage = new Mage("Mage");

        TreeMap<AttributeConstants, Integer> clothArmorStats = new TreeMap<>();
        clothArmorStats.put(AttributeConstants.vitality, 4);
        clothArmorStats.put(AttributeConstants.strength, 1);
        clothArmorStats.put(AttributeConstants.dexterity, 4);
        clothArmorStats.put(AttributeConstants.intelligence, 4);

        TreeMap<AttributeConstants, Integer> clothHatStats = new TreeMap<>();
        clothHatStats.put(AttributeConstants.vitality, 2);
        clothHatStats.put(AttributeConstants.strength, 3);
        clothHatStats.put(AttributeConstants.dexterity, 1);
        clothHatStats.put(AttributeConstants.intelligence, 7);

        TreeMap<AttributeConstants, Integer> clothPantsStats = new TreeMap<>();
        clothPantsStats.put(AttributeConstants.vitality, 4);
        clothPantsStats.put(AttributeConstants.strength, 1);
        clothPantsStats.put(AttributeConstants.dexterity, 1);
        clothPantsStats.put(AttributeConstants.intelligence, 5);

        Weapon staffOfDoom = new Weapon("Staff of Doom", 5, Slot.Weapon, WeaponType.Staff, 12.0, 0.3);
        Armor wizardRobe = new Armor("Wizard Robe", 3, Slot.Body, ArmorType.Cloth, clothArmorStats);
        Armor wizardPants = new Armor("Wizard Pants", 2, Slot.Legs, ArmorType.Cloth, clothPantsStats);
        Armor wizardHat = new Armor("Wizard's Hat", 3, Slot.Head, ArmorType.Cloth, clothHatStats);

        int i;
        for (i = 0; i < 9; i++) {
            mage.levelUp();
        }
        try {
            mage.equipArmor(wizardHat);
            mage.equipArmor(wizardPants);
            mage.equipArmor(wizardRobe);
        } catch(InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        try {
            mage.equipWeapon(staffOfDoom);
        } catch(InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        double getDpsWithLevelsAndEquipment = mage.getDamagePerSecond();

        double itemDpsBonus = 0;
        int intelligenceGain = 5;
        int startingInt = 8;
        for (Item equipment : mage.getCharacterEquipment().values()) {
            if(equipment.getArmorType() != null) {
                itemDpsBonus += ((Armor)equipment).getAttributes().get(AttributeConstants.intelligence);
            }
            if(equipment.getWeaponType() != null) {
                itemDpsBonus += ((Weapon)equipment).getWeaponDamagePerSecond();
            }
        }
        assertEquals((i*intelligenceGain)+startingInt+itemDpsBonus, getDpsWithLevelsAndEquipment);
    }
    @Test
    void isWarriorDPSUpdatingProperly_equippedItemsAndLevel_correctCalculations() {
        var warrior = new Warrior("Warrior");

        TreeMap<AttributeConstants, Integer> plateArmorStats = new TreeMap<>();
        plateArmorStats.put(AttributeConstants.vitality, 4);
        plateArmorStats.put(AttributeConstants.strength, 4);
        plateArmorStats.put(AttributeConstants.dexterity, 1);
        plateArmorStats.put(AttributeConstants.intelligence, 1);

        TreeMap<AttributeConstants, Integer> plateLegsStats = new TreeMap<>();
        plateLegsStats.put(AttributeConstants.vitality, 4);
        plateLegsStats.put(AttributeConstants.strength, 3);
        plateLegsStats.put(AttributeConstants.dexterity, 1);
        plateLegsStats.put(AttributeConstants.intelligence, 1);

        TreeMap<AttributeConstants, Integer> plateHelmStats = new TreeMap<>();
        plateHelmStats.put(AttributeConstants.vitality, 2);
        plateHelmStats.put(AttributeConstants.strength, 5);
        plateHelmStats.put(AttributeConstants.dexterity, 2);
        plateHelmStats.put(AttributeConstants.intelligence, 1);

        Weapon mightyAxe = new Weapon("Mighty Axe", 5, Slot.Weapon, WeaponType.Axe, 10, 0.4);
        Armor plateArmor = new Armor("Warrior's Chestpiece of Might", 3, Slot.Body, ArmorType.Plate, plateArmorStats);
        Armor plateLegs = new Armor("Warrior's Chain Legs", 3, Slot.Legs, ArmorType.Plate, plateLegsStats);
        Armor plateHelm = new Armor("Warrior's Plate Helm", 8, Slot.Head, ArmorType.Plate, plateHelmStats);

        int i;
        for (i = 0; i < 5; i++) {
            warrior.levelUp();
        }
        try {
            warrior.equipArmor(plateArmor);
            warrior.equipArmor(plateLegs);
            warrior.equipArmor(plateHelm);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        try {
            warrior.equipWeapon(mightyAxe);
        } catch(InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        double getDpsWithLevelsAndEquipment = warrior.getDamagePerSecond();

        double itemDpsBonus = 0;
        int strengthGain = 3;
        int startingStrength = 5;
        for (Item equipment : warrior.getCharacterEquipment().values()) {
            if(equipment.getArmorType() != null) {
                itemDpsBonus += ((Armor)equipment).getAttributes().get(AttributeConstants.strength);
            }
            if(equipment.getWeaponType() != null) {
                itemDpsBonus += ((Weapon)equipment).getWeaponDamagePerSecond();
            }
        }
        warrior.listEquippedItems();
        assertEquals(i*strengthGain+startingStrength+itemDpsBonus, getDpsWithLevelsAndEquipment);
    }

    @Test
    void isRogueDPSUpdatingProperly_equippedItemsAndLevel_correctCalculations() {
        var rogue = new Rogue("Rogue");

        TreeMap<AttributeConstants, Integer> leatherArmorStats = new TreeMap<>();
        leatherArmorStats.put(AttributeConstants.vitality, 4);
        leatherArmorStats.put(AttributeConstants.strength, 4);
        leatherArmorStats.put(AttributeConstants.dexterity, 1);
        leatherArmorStats.put(AttributeConstants.intelligence, 1);

        TreeMap<AttributeConstants, Integer> leatherLegsStats = new TreeMap<>();
        leatherLegsStats.put(AttributeConstants.vitality, 4);
        leatherLegsStats.put(AttributeConstants.strength, 3);
        leatherLegsStats.put(AttributeConstants.dexterity, 1);
        leatherLegsStats.put(AttributeConstants.intelligence, 1);

        TreeMap<AttributeConstants, Integer> mailHelmStats = new TreeMap<>();
        mailHelmStats.put(AttributeConstants.vitality, 2);
        mailHelmStats.put(AttributeConstants.strength, 5);
        mailHelmStats.put(AttributeConstants.dexterity, 2);
        mailHelmStats.put(AttributeConstants.intelligence, 1);

        Weapon sharpDagger = new Weapon("Sharp Dagger", 3, Slot.Weapon, WeaponType.Dagger, 3, 2);
        Armor leatherArmor = new Armor("Leather Armor", 10, Slot.Body, ArmorType.Leather, leatherArmorStats);
        Armor leatherLegs = new Armor("Leather Pants", 7, Slot.Legs, ArmorType.Leather, leatherLegsStats);
        Armor mailHelm = new Armor("Chain Helm", 8, Slot.Head, ArmorType.Mail, mailHelmStats);

        int i;
        for (i = 0; i < 9; i++) {
            rogue.levelUp();
        }
        try {
            rogue.equipArmor(leatherArmor);
            rogue.equipArmor(leatherLegs);
            rogue.equipArmor(mailHelm);
        } catch(InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        try {
            rogue.equipWeapon(sharpDagger);
        } catch(InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        double getDpsWithLevelsAndEquipment = rogue.getDamagePerSecond();

        double itemDpsBonus = 0;
        int dexterityGain = 4;
        int startingDex = 6;
        for (Item equipment : rogue.getCharacterEquipment().values()) {
            if(equipment.getArmorType() != null) {
                itemDpsBonus += ((Armor)equipment).getAttributes().get(AttributeConstants.dexterity);
            }
            if(equipment.getWeaponType() != null) {
                itemDpsBonus += ((Weapon)equipment).getWeaponDamagePerSecond();
            }
        }
        assertEquals((i*dexterityGain)+startingDex+itemDpsBonus, getDpsWithLevelsAndEquipment);
    }

    @Test
    void isRangerDPSUpdatingProperly_equippedItemsAndLevel_correctCalculations() {
        var ranger = new Ranger("Ranger");

        TreeMap<AttributeConstants, Integer> leatherArmorStats = new TreeMap<>();
        leatherArmorStats.put(AttributeConstants.vitality, 4);
        leatherArmorStats.put(AttributeConstants.strength, 4);
        leatherArmorStats.put(AttributeConstants.dexterity, 1);
        leatherArmorStats.put(AttributeConstants.intelligence, 1);

        TreeMap<AttributeConstants, Integer> leatherLegsStats = new TreeMap<>();
        leatherLegsStats.put(AttributeConstants.vitality, 4);
        leatherLegsStats.put(AttributeConstants.strength, 3);
        leatherLegsStats.put(AttributeConstants.dexterity, 1);
        leatherLegsStats.put(AttributeConstants.intelligence, 1);

        TreeMap<AttributeConstants, Integer> mailHelmStats = new TreeMap<>();
        mailHelmStats.put(AttributeConstants.vitality, 2);
        mailHelmStats.put(AttributeConstants.strength, 5);
        mailHelmStats.put(AttributeConstants.dexterity, 2);
        mailHelmStats.put(AttributeConstants.intelligence, 1);

        Weapon huntingBw = new Weapon("Hunting Bow", 3, Slot.Weapon, WeaponType.Bow, 20, 0.5);
        Armor leatherArmor = new Armor("Leather Armor", 10, Slot.Body, ArmorType.Leather, leatherArmorStats);
        Armor leatherLegs = new Armor("Leather Pants", 7, Slot.Legs, ArmorType.Leather, leatherLegsStats);
        Armor mailHelm = new Armor("Chain Helm", 8, Slot.Head, ArmorType.Mail, mailHelmStats);
        int i;
        for (i = 0; i < 9; i++) {
            ranger.levelUp();
        }
        try {
            ranger.equipArmor(leatherArmor);
            ranger.equipArmor(leatherLegs);
            ranger.equipArmor(mailHelm);
        } catch(InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        try {
            ranger.equipWeapon(huntingBw);
        } catch(InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }

        double getDpsWithLevelsAndEquipment = ranger.getDamagePerSecond();
        double itemDpsBonus = 0;
        int dexterityGain = 5;
        int startingDex = 7;
        for (Item equipment : ranger.getCharacterEquipment().values()) {
            if(equipment.getArmorType() != null) {
                itemDpsBonus += ((Armor)equipment).getAttributes().get(AttributeConstants.dexterity);
            }
            if(equipment.getWeaponType() != null) {
                itemDpsBonus += ((Weapon)equipment).getWeaponDamagePerSecond();
            }
        }
        assertEquals((i*dexterityGain)+startingDex+itemDpsBonus, getDpsWithLevelsAndEquipment);
    }
}