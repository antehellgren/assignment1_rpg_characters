package com.company.character.classes;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class LevelTest {
    @Test
    void isCharacterStartingAtLevelOne_newCharacter_correctLevel() {
        var mage = new Mage("Mage");
        int level = mage.getLevel();
        assertEquals(1, level);
    }
    @Test
    void isCharacterLevelingUpProperly_increaseLevel_correctLevel() {
        var mage = new Mage("Mage");
        mage.levelUp();
        int level = mage.getLevel();
        assertEquals(2, level);
    }
    @Test
    void isExceptionThrownWhenLevelIncreasedByZero_increaseByZero_argumentThrown() {
        var mage = new Mage("Mage");
        assertThrows(IllegalArgumentException.class, () -> mage.setLevel(0));
    }
}
