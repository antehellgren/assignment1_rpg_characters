package com.company.character.classes;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SecondaryAttributeTest {
    @Test
    void isSecondaryAttributesUpdating_whenLevelUp_correctNumbers() {
        var warrior = new Warrior("Warrior");
        warrior.levelUp();
        boolean isAttributesCorrect = false;
        if(warrior.getSecondaryAttributes().getHealth() == 150 &&
            warrior.getSecondaryAttributes().getArmorRating() == 12 &&
            warrior.getSecondaryAttributes().getElementalResistance() == 2)
                isAttributesCorrect = true;
            assertTrue(isAttributesCorrect);
    }
}